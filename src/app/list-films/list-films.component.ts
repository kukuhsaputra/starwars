import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-list-films',
  templateUrl: './list-films.component.html',
  styleUrls: ['./list-films.component.css']
})
export class ListFilmsComponent implements OnInit {
  id: string;
  public d: any;
  constructor(private activatedRoute: ActivatedRoute, private http: HttpClient,) {
    this.id = this.activatedRoute.snapshot.params.id;
  }

  ngOnInit() {
    this.show()
  }

  show() {
    this.http.get<any>("http://localhost:3000/films/" + this.id).subscribe(response => {
      this.d = response;
    });
  }

}

import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {
  public films: Array<any> = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.peluangPendanaan()
  }

  peluangPendanaan() {
    this.http.get<any>('http://localhost:3000/films').subscribe(response => {
      for (let i = 0; i < response.length; i++) {
        this.films.push(response[+i]);
      }
    });
  }

}

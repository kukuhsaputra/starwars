import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FilmsComponent} from './films/films.component';
import {RouterModule, Routes} from '@angular/router';
import { PeopleComponent } from './people/people.component';
import {HttpClientModule} from '@angular/common/http';
import { ListFilmsComponent } from './list-films/list-films.component';

const routes: Routes = [
  {path: 'films', component: FilmsComponent},
  {path: 'film/:id', component: ListFilmsComponent},
  {path: 'people', component: PeopleComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    FilmsComponent,
    PeopleComponent,
    ListFilmsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

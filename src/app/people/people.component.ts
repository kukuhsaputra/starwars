import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {
  public peoples: Array<any> = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.peluangPendanaan()
  }

  peluangPendanaan() {
    this.http.get<any>('http://localhost:3000/people').subscribe(response => {
      for (let i = 0; i < response.length; i++) {
        this.peoples.push(response[+i]);
      }
    });
  };
}
